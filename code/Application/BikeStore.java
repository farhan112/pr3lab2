//Farhan Khandaker 2135266
package Application;

public class BikeStore {
    public static void main(String[] args) {
        
        vehicles.Bicycle[] bicycles = new vehicles.Bicycle[4];
        bicycles[0] = new vehicles.Bicycle("BikeMakers", 11, 55);
        bicycles[1] = new vehicles.Bicycle("Ecobike", 22, 66);
        bicycles[2] = new vehicles.Bicycle("Bicyclops", 33, 77);
        bicycles[3] = new vehicles.Bicycle("NotJustBikes", 44, 88);
    
        for(int i = 0; i < bicycles.length; i++){
            System.out.println(bicycles[i]);
        }
    }
}
